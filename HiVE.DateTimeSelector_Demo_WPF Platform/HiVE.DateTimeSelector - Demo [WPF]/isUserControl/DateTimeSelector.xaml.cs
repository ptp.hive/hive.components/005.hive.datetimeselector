﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using System;
using System.Windows.Controls;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.DateTimeSelector_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for DateTimeSelector.xaml
    /// </summary>
    internal partial class DateTimeSelector : UserControl
    {
        public DateTimeSelector()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DateTimeSelector_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DateTimeSelector_Demo_WPF_IsUserControl.DateTimeSelector).ToString();

        private static TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WPF_IsUserControl_DateTimeSelector TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        #region CheckBoxs FlowDirection

        private void checkBoxFlowDirection_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                WPF_DatePicker.FlowDirection = System.Windows.FlowDirection.RightToLeft;
            }
            catch { }
        }

        private void checkBoxFlowDirection_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                WPF_DatePicker.FlowDirection = System.Windows.FlowDirection.LeftToRight;
            }
            catch { }
        }

        private void checkBoxRightToLeftLayout_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                WinForms_DateTimePicker.RightToLeftLayout = checkBoxRightToLeftLayout.IsChecked ?? false;
            }
            catch { }
        }

        private void checkBoxRightToLeftLayout_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                WinForms_DateTimePicker.RightToLeftLayout = checkBoxRightToLeftLayout.IsChecked ?? false;
            }
            catch { }
        }

        private void checkBoxUsePersianFormat_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                dateTimeSelectorMain.UsePersianFormat = checkBoxUsePersianFormat.IsChecked ?? false;
            }
            catch { }
        }

        private void checkBoxUsePersianFormat_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                dateTimeSelectorMain.UsePersianFormat = checkBoxUsePersianFormat.IsChecked ?? false;
            }
            catch { }
        }

        #endregion

        private void dateTimeSelector_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WPF_IsUserControl_DateTimeSelector.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                dateTimeSelectorMain.UsePersianFormat = checkBoxUsePersianFormat.IsChecked ?? false;
                dateTimeSelectorMain.Value = System.DateTime.Now;
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
