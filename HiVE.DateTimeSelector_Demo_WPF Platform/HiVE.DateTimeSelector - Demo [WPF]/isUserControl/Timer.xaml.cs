﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using HiVE.DateTimeSelector;
using System;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.DateTimeSelector_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for Timer.xaml
    /// </summary>
    internal partial class Timer : UserControl
    {
        public Timer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DateTimeSelector_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DateTimeSelector_Demo_WPF_IsUserControl.Timer).ToString();

        private TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WPF_IsUserControl_Timer TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        #region Fields

        bool status_Timer_IsReset = false;
        private BoxTimer timerStatus = new BoxTimer();
        TimeSpan maxTimeValue_Status = new TimeSpan();
        DispatcherTimer dispatcherTimer_Timer = new DispatcherTimer();

        #endregion

        #region MultiThreaded

        private Thread trTimer_Timer;
        private ThreadStart thrsTimer_Timer;

        #endregion

        #region Timer Status

        private void timer_Timer_Reset()
        {
            try
            {
                maxTimeValue_Status =
                    new TimeSpan(
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(0, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(3, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(6, 2))
                        );

                timerStatus = new BoxTimer(
                    new TimeSpan(0),
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    dispatcherTimer_Timer.Interval,
                    1,
                    new TimeSpan(0),
                    new TimeSpan(0),
                    new TimeSpan(0));

                Dispatcher.Invoke(new Action(delegate
                {
                    textBlockTimer_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        maxTimeValue_Status.Hours,
                        maxTimeValue_Status.Minutes,
                        maxTimeValue_Status.Seconds,
                        maxTimeValue_Status.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Timer_Start()
        {
            try
            {
                if (status_Timer_IsReset)
                {
                    timer_Timer_Reset();
                    status_Timer_IsReset = false;
                }

                maxTimeValue_Status =
                    new TimeSpan(
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(0, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(3, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(6, 2))
                        );

                timerStatus = new BoxTimer(
                    timerStatus.LastStopwatchValueBeforePause,
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    dispatcherTimer_Timer.Interval,
                    1,
                    timerStatus.StopwatchValue,
                    maxTimeValue_Status,
                    timerStatus.TimerValue);

                dispatcherTimer_Timer.IsEnabled = true;
            }
            catch { }
        }

        private void timer_Timer_Counter()
        {
            try
            {
                timerStatus =
                    BoxBasicTicks.Timer_Tick(timerStatus);

                Dispatcher.Invoke(new Action(delegate
                {
                    textBlockTimer_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        timerStatus.TimerValue.Hours,
                        timerStatus.TimerValue.Minutes,
                        timerStatus.TimerValue.Seconds,
                        timerStatus.TimerValue.Milliseconds);
                }));

                if (timerStatus.TimerValue.Ticks <= 0)
                {
                    timer_Timer_Stop();

                    dispatcherTimer_Timer_EnabledChanged(dispatcherTimer_Timer.IsEnabled);
                    timer_Timer_Lap();

                    buttonStartStop.IsEnabled = false;
                }
            }
            catch { }
        }

        private void dispatcherTimer_Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                thrsTimer_Timer = new ThreadStart(timer_Timer_Counter);
                trTimer_Timer = new Thread(thrsTimer_Timer);
                trTimer_Timer.Priority = ThreadPriority.AboveNormal;
                //
                trTimer_Timer.Start();
            }
            catch { }
        }

        private void timer_Timer_Stop()
        {
            try
            {
                dispatcherTimer_Timer.IsEnabled = false;

                timerStatus =
                    BoxBasicTicks.Timer_Tick(timerStatus);

                timerStatus.LastStopwatchValueBeforePause = timerStatus.StopwatchValue;

                Dispatcher.Invoke(new Action(delegate
                {
                    textBlockTimer_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        timerStatus.TimerValue.Hours,
                        timerStatus.TimerValue.Minutes,
                        timerStatus.TimerValue.Seconds,
                        timerStatus.TimerValue.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Timer_Lap()
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    string strStatusProcess = string.Empty;
                    if (dispatcherTimer_Timer.IsEnabled)
                    {
                        strStatusProcess = "Started";
                    }
                    else
                    {
                        strStatusProcess = "Stoped";
                    }

                    listBoxTimer_LogLap.Items.Add(
                        string.Format(
                            "Process {0}, In Time [{1}]! Status is => [{2}:{3}:{4}.{5}]!",
                            strStatusProcess,
                            DateTime.Now,
                            timerStatus.TimerValue.Hours,
                            timerStatus.TimerValue.Minutes,
                            timerStatus.TimerValue.Seconds,
                            timerStatus.TimerValue.Milliseconds));
                }));
            }
            catch { }
        }

        #endregion

        #region Timer Functions

        private void checkBoxMilliseconds_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    dispatcherTimer_Timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                }));
            }
            catch { }
        }

        private void checkBoxMilliseconds_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    dispatcherTimer_Timer.Interval = new TimeSpan(0, 0, 0, 1, 0);
                }));
            }
            catch { }
        }

        private bool dispatcherTimer_Timer_EnabledChanged(bool isEnabled)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    checkBoxMilliseconds.IsEnabled = !isEnabled;
                    maskedtxtMaxTimeValue.Enabled = !isEnabled;
                }));

                if (isEnabled)
                {
                    Dispatcher.Invoke(new Action(delegate
                    {
                        buttonResetLap.Content = "Lap";
                    }));
                }
                else
                {
                    Dispatcher.Invoke(new Action(delegate
                    {
                        buttonResetLap.Content = "Reset";
                    }));
                }
            }
            catch { return false; }

            return true;
        }

        private void buttonStartStop_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (!dispatcherTimer_Timer.IsEnabled)
                {
                    timer_Timer_Start();

                    dispatcherTimer_Timer_EnabledChanged(dispatcherTimer_Timer.IsEnabled);
                    timer_Timer_Lap();
                }
                else
                {
                    timer_Timer_Stop();

                    dispatcherTimer_Timer_EnabledChanged(dispatcherTimer_Timer.IsEnabled);
                    timer_Timer_Lap();
                }
            }
            catch { }
        }

        private void buttonResetLap_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (dispatcherTimer_Timer.IsEnabled)
                {
                    timer_Timer_Lap();
                }
                else
                {
                    timer_Timer_Reset();
                    buttonStartStop.IsEnabled = true;
                }
            }
            catch { }
        }

        #endregion

        private void timer_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WPF_IsUserControl_Timer.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                Dispatcher.Invoke(new Action(delegate
                {
                    bool newBoolIsChecked = checkBoxMilliseconds.IsChecked ?? false;
                    if (newBoolIsChecked)
                    {
                        dispatcherTimer_Timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                    }
                    else
                    {
                        dispatcherTimer_Timer.Interval = new TimeSpan(0, 0, 0, 1, 0);
                    }
                }));

                dispatcherTimer_Timer.Tick += new EventHandler(this.dispatcherTimer_Timer_Tick);

                dispatcherTimer_Timer_EnabledChanged(dispatcherTimer_Timer.IsEnabled);
                status_Timer_IsReset = true;
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
