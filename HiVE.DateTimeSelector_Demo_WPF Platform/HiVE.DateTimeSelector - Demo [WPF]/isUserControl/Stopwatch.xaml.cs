﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using HiVE.DateTimeSelector;
using System;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.DateTimeSelector_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for Stopwatch.xaml
    /// </summary>
    internal partial class Stopwatch : UserControl
    {
        public Stopwatch()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DateTimeSelector_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DateTimeSelector_Demo_WPF_IsUserControl.Stopwatch).ToString();

        private TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WPF_IsUserControl_Stopwatch TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        #region Fields

        bool status_Stopwatch_IsReset = false;
        private BoxStopwatch stopwatchStatus = new BoxStopwatch();
        DispatcherTimer dispatcherTimer_Stopwatch = new DispatcherTimer();

        #endregion

        #region MultiThreaded

        private Thread trTimer_Stopwatch;
        private ThreadStart thrsTimer_Stopwatch;

        #endregion

        #region Stopwatch Status

        private void timer_Stopwatch_Reset()
        {
            try
            {
                stopwatchStatus = new BoxStopwatch(
                    new TimeSpan(0),
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    dispatcherTimer_Stopwatch.Interval,
                    1,
                    new TimeSpan(0));

                Dispatcher.Invoke(new Action(delegate
                {
                    textBlockStopwatch_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        stopwatchStatus.StopwatchValue.Hours,
                        stopwatchStatus.StopwatchValue.Minutes,
                        stopwatchStatus.StopwatchValue.Seconds,
                        stopwatchStatus.StopwatchValue.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Stopwatch_Start()
        {
            try
            {
                if (status_Stopwatch_IsReset)
                {
                    timer_Stopwatch_Reset();
                    status_Stopwatch_IsReset = false;
                }

                stopwatchStatus = new BoxStopwatch(
                    stopwatchStatus.LastStopwatchValueBeforePause,
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    dispatcherTimer_Stopwatch.Interval,
                    1,
                    stopwatchStatus.StopwatchValue);

                dispatcherTimer_Stopwatch.IsEnabled = true;
            }
            catch { }
        }

        private void timer_Stopwatch_Counter()
        {
            try
            {
                stopwatchStatus =
                    BoxBasicTicks.Stopwatch_Tick(stopwatchStatus);

                Dispatcher.Invoke(new Action(delegate
                {
                    textBlockStopwatch_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        stopwatchStatus.StopwatchValue.Hours,
                        stopwatchStatus.StopwatchValue.Minutes,
                        stopwatchStatus.StopwatchValue.Seconds,
                        stopwatchStatus.StopwatchValue.Milliseconds);
                }));

                //Dispatcher.Invoke(new Action(delegate { progressBarStatus.Value += 1; }));
            }
            catch { }
        }

        private void dispatcherTimer_Stopwatch_Tick(object sender, EventArgs e)
        {
            try
            {
                thrsTimer_Stopwatch = new ThreadStart(timer_Stopwatch_Counter);
                trTimer_Stopwatch = new Thread(thrsTimer_Stopwatch);
                trTimer_Stopwatch.Priority = ThreadPriority.AboveNormal;
                //
                trTimer_Stopwatch.Start();
            }
            catch { }
        }

        private void timer_Stopwatch_Stop()
        {
            try
            {
                dispatcherTimer_Stopwatch.IsEnabled = false;

                stopwatchStatus =
                    BoxBasicTicks.Stopwatch_Tick(stopwatchStatus);

                stopwatchStatus.LastStopwatchValueBeforePause = stopwatchStatus.StopwatchValue;

                Dispatcher.Invoke(new Action(delegate
                {
                    textBlockStopwatch_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        stopwatchStatus.StopwatchValue.Hours,
                        stopwatchStatus.StopwatchValue.Minutes,
                        stopwatchStatus.StopwatchValue.Seconds,
                        stopwatchStatus.StopwatchValue.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Stopwatch_Lap()
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    string strStatusProcess = string.Empty;
                    if (dispatcherTimer_Stopwatch.IsEnabled)
                    {
                        strStatusProcess = "Started";
                    }
                    else
                    {
                        strStatusProcess = "Stoped";
                    }

                    listBoxStopwatch_LogLap.Items.Add(
                        string.Format(
                            "Process {0}, In Time [{1}]! Status is => [{2}:{3}:{4}.{5}]!",
                            strStatusProcess,
                            DateTime.Now,
                            stopwatchStatus.StopwatchValue.Hours,
                            stopwatchStatus.StopwatchValue.Minutes,
                            stopwatchStatus.StopwatchValue.Seconds,
                            stopwatchStatus.StopwatchValue.Milliseconds));
                }));
            }
            catch { }
        }

        #endregion

        #region Stopwatch Functions

        private void checkBoxMilliseconds_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    dispatcherTimer_Stopwatch.Interval = new TimeSpan(0, 0, 0, 0, 1);
                }));
            }
            catch { }
        }

        private void checkBoxMilliseconds_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    dispatcherTimer_Stopwatch.Interval = new TimeSpan(0, 0, 0, 1, 0);
                }));
            }
            catch { }
        }

        private bool dispatcherTimer_Stopwatch_EnabledChanged(bool isEnabled)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    checkBoxMilliseconds.IsEnabled = !isEnabled;
                }));

                if (isEnabled)
                {
                    Dispatcher.Invoke(new Action(delegate
                    {
                        buttonResetLap.Content = "Lap";
                    }));
                }
                else
                {
                    Dispatcher.Invoke(new Action(delegate
                    {
                        buttonResetLap.Content = "Reset";
                    }));
                }
            }
            catch { return false; }

            return true;
        }

        private void buttonStartStop_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (!dispatcherTimer_Stopwatch.IsEnabled)
                {
                    timer_Stopwatch_Start();

                    dispatcherTimer_Stopwatch_EnabledChanged(dispatcherTimer_Stopwatch.IsEnabled);
                    timer_Stopwatch_Lap();
                }
                else
                {
                    timer_Stopwatch_Stop();

                    dispatcherTimer_Stopwatch_EnabledChanged(dispatcherTimer_Stopwatch.IsEnabled);
                    timer_Stopwatch_Lap();
                }
            }
            catch { }
        }

        private void buttonResetLap_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (dispatcherTimer_Stopwatch.IsEnabled)
                {
                    timer_Stopwatch_Lap();
                }
                else
                {
                    timer_Stopwatch_Reset();
                }
            }
            catch { }
        }

        #endregion

        private void stopwatch_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WPF_IsUserControl_Stopwatch.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                Dispatcher.Invoke(new Action(delegate
                {
                    bool newBoolIsChecked = checkBoxMilliseconds.IsChecked ?? false;
                    if (newBoolIsChecked)
                    {
                        dispatcherTimer_Stopwatch.Interval = new TimeSpan(0, 0, 0, 0, 1);
                    }
                    else
                    {
                        dispatcherTimer_Stopwatch.Interval = new TimeSpan(0, 0, 0, 1, 0);
                    }
                }));

                dispatcherTimer_Stopwatch.Tick += new EventHandler(this.dispatcherTimer_Stopwatch_Tick);

                dispatcherTimer_Stopwatch_EnabledChanged(dispatcherTimer_Stopwatch.IsEnabled);
                status_Stopwatch_IsReset = true;
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
