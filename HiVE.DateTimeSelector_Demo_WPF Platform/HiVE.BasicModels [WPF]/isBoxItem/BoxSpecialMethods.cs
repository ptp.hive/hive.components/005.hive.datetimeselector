﻿using System.ComponentModel;

namespace HiVE.BasicModels.isBoxItem
{
    /// <summary>
    /// BoxSpecialMethods for this [HiVE.DateTimeSelector - Demo [WPF]] HiVE project.
    /// </summary>
    public static class BoxSpecialMethods
    {
        /// <summary>
        /// Get Special ProductInformation
        /// </summary>
        public enum MethodProductInformation
        {
            [SpecialDescription("HiVE.DateTimeSelector - Demo [WPF]")]
            [Description("HiVE.DateTimeSelector - Demo [WPF]")]
            ProductName,
            [SpecialDescription("http://www.HiVE.DateTimeSelector.Demo.WPF.com/")]
            [Description("www.HiVE.DateTimeSelector.Demo.WPF.com")]
            ProductWebSite,
            [SpecialDescription("http://HiVE.DateTimeSelector.Demo.WPF@gmail.com/")]
            [Description("HiVE.DateTimeSelector.Demo.WPF@gmail.com")]
            ProductEmail,
            [SpecialDescription("http://www.PTP.HiVE.com/HiVE.DateTimeSelector.Demo.WPF/")]
            [Description("www.PTP.HiVE.com/HiVE.DateTimeSelector.Demo.WPF")]
            ProductLink,
        }
    }
}
