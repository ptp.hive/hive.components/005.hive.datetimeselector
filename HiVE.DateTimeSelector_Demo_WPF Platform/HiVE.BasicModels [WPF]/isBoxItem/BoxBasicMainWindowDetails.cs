﻿using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;

namespace HiVE.BasicModels.isBoxItem
{
    public class BoxBasicWindowDetails
    {
        public string MainMenuContent { get; set; }

        public string AssemblyProductVersionMajor { get; set; }
        public string AssemblyCopyright { get; set; }

        public string ProductIsTrialMode { get; set; }

        public MethodCulture BasicCulture { get; set; }

        public BoxBasicWindowDetails() { }

        public BoxBasicWindowDetails(
            string mainMenuContent,

            string assemblyProductVersionMajor,
            string assemblyCopyright,

            string productIsTrialMode,

            MethodCulture basicCulture)
        {
            this.MainMenuContent = mainMenuContent;

            this.AssemblyProductVersionMajor = assemblyProductVersionMajor;
            this.AssemblyCopyright = assemblyCopyright;

            this.ProductIsTrialMode = productIsTrialMode;

            this.BasicCulture = basicCulture;
        }
    }
}

