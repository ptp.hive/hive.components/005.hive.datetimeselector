# 005.HiVE.DateTimeSelector

***[005.HiVE.DateTimeSelector]***

Global Common **DateTime Selector** for the HiVE Productions [WPF/WinForms].

A WPF/WinForms *Date/Time selector* component which is part of the *Atf UI Library*.