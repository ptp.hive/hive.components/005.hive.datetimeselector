﻿using System;
using static HiVE.DateTimeSelector.BoxSpecialMethods;

namespace HiVE.DateTimeSelector.Models
{
    internal static class Timer
    {
        #region Methods

        /// <summary>
        /// Timer Tick
        /// CountDown Time
        /// </summary>
        /// <param name="boxTimer"></param>
        /// <returns></returns>
        public static BoxTimer Timer_Tick(BoxTimer boxTimer)
        {
            TimeSpan timeDifference =
                DateTime.UtcNow.Subtract(boxTimer.LastTimeStartUtcValue) -
                boxTimer.LastTimeValue;

            int _intervalTick = (int)boxTimer.IntervalTick.TotalMilliseconds;

            if (timeDifference.TotalMilliseconds > _intervalTick - (_intervalTick * BoxSpecialMethods.threshold_Milliseconds / 100) &&
                timeDifference.TotalMilliseconds < _intervalTick + (_intervalTick * BoxSpecialMethods.threshold_Milliseconds / 100))
            {
                int milliseconds = boxTimer.LastTimeValue.Milliseconds;

                if (boxTimer.NextStepTick != 0)
                {
                    milliseconds += (_intervalTick * boxTimer.NextStepTick);
                }

                boxTimer.LastTimeValue =
                    new TimeSpan(
                        boxTimer.LastTimeValue.Days,
                        boxTimer.LastTimeValue.Hours,
                        boxTimer.LastTimeValue.Minutes,
                        boxTimer.LastTimeValue.Seconds,
                        milliseconds);
            }
            else
            {
                boxTimer.LastTimeValue = DateTime.UtcNow.Subtract(boxTimer.LastTimeStartUtcValue);
            }

            boxTimer.StopwatchValue = boxTimer.LastTimeValue + boxTimer.LastStopwatchValueBeforePause;

            boxTimer.TimerValue = boxTimer.MaxTimeValue.Subtract(boxTimer.StopwatchValue);
            if (boxTimer.TimerValue.Ticks < 0)
            {
                boxTimer.TimerValue = new TimeSpan(0);
            }

            return boxTimer;
        }

        public static int DecreaseTimerValue(SpecifierInfo specifier, int value)
        {
            switch (specifier.Type)
            {
                case SpecifierType.ms:
                case SpecifierType.mss:
                    {
                        if (value > 0)
                        {
                            value--;
                        }
                        else
                        {
                            value = 999;
                        }
                        break;
                    }

                case SpecifierType.s:
                case SpecifierType.ss:
                    {
                        if (value > 0)
                        {
                            value--;
                        }
                        else
                        {
                            value = 59;
                        }
                        break;
                    }

                case SpecifierType.m:
                case SpecifierType.mm:
                    {
                        if (value > 0)
                        {
                            value--;
                        }
                        else
                        {
                            value = 59;
                        }
                        break;
                    }

                case SpecifierType.h:
                case SpecifierType.hh:
                case SpecifierType.H:
                case SpecifierType.HH:
                    {
                        if (value > 0)
                        {
                            value--;
                        }
                        else
                        {
                            value = 0;
                        }
                        break;
                    }

                default:
                    {
                        value = -1;

                        break;
                    }
            }

            return value;
        }

        public static void DecreaseTimerValue_StepByStep(TimeSpan value, bool showMillisecond)
        {
            int milliseconds = value.Milliseconds;
            int seconds = value.Seconds;
            int minutes = value.Minutes;
            int hours = value.Hours;
            int days = value.Days;

            if (showMillisecond)
            {
                /// Define milliseconds if(milliseconds == 0)
                if (milliseconds == 0 && seconds > 0)
                {
                    seconds--;
                    milliseconds = 999;
                }
                else if (seconds == 0 && minutes > 0)
                {
                    minutes--;
                    seconds = 59; milliseconds = 999;
                }
                else if (seconds == 0 && minutes == 0 && hours > 0)
                {
                    hours--;
                    minutes = 59; seconds = 59; milliseconds = 999;
                }
                else if (seconds == 0 && minutes == 0 && hours == 0 && days > 0)
                {
                    days--;
                    hours = 23; minutes = 59; seconds = 59; milliseconds = 999;
                }
            }

            /// Define seconds if(seconds == 0)
            if (seconds == 0 && minutes > 0)
            {
                minutes--;
                seconds = 59;
            }
            else if (seconds == 0 && minutes == 0 && hours > 0)
            {
                hours--;
                minutes = 59; seconds = 59;
            }
            else if (seconds == 0 && minutes == 0 && hours == 0 && days > 0)
            {
                days--;
                hours = 23; minutes = 59; seconds = 59;
            }

            /// Define minutes if(minutes == 0)
            if (minutes == 0 && hours > 0)
            {
                hours--;
                minutes = 59;
            }
            else if (minutes == 0 && hours == 0 && days > 0)
            {
                days--;
                hours = 23; minutes = 59;
            }

            /// Define hours if(hours == 0)
            if (hours == 0 && days > 0)
            {
                days--;
                hours = 23;
            }

            /// Check Stop Condition
            if (hours == 0 && minutes == 0 && seconds == 0 && milliseconds == 0)
            {
                //timer1.Stop();
            }

        }

        #endregion
    }
}
