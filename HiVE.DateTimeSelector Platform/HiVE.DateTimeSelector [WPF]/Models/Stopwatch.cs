﻿using System;
using static HiVE.DateTimeSelector.BoxSpecialMethods;

namespace HiVE.DateTimeSelector.Models
{
    internal class Stopwatch
    {
        #region Methods

        /// <summary>
        /// Stopwatch Tick
        /// CountUp Time
        /// </summary>
        /// <param name="boxStopwatch"></param>
        /// <returns></returns>
        public static BoxStopwatch Stopwatch_Tick(BoxStopwatch boxStopwatch)
        {
            TimeSpan timeDifference =
                DateTime.UtcNow.Subtract(boxStopwatch.LastTimeStartUtcValue) -
                boxStopwatch.LastTimeValue;

            int _intervalTick = (int)boxStopwatch.IntervalTick.TotalMilliseconds;

            if (timeDifference.TotalMilliseconds > _intervalTick - (_intervalTick * BoxSpecialMethods.threshold_Milliseconds / 100) &&
                timeDifference.TotalMilliseconds < _intervalTick + (_intervalTick * BoxSpecialMethods.threshold_Milliseconds / 100))
            {
                int milliseconds = boxStopwatch.LastTimeValue.Milliseconds;

                if (boxStopwatch.NextStepTick != 0)
                {
                    milliseconds += (_intervalTick * boxStopwatch.NextStepTick);
                }

                boxStopwatch.LastTimeValue =
                    new TimeSpan(
                        boxStopwatch.LastTimeValue.Days,
                        boxStopwatch.LastTimeValue.Hours,
                        boxStopwatch.LastTimeValue.Minutes,
                        boxStopwatch.LastTimeValue.Seconds,
                        milliseconds);
            }
            else
            {
                boxStopwatch.LastTimeValue = DateTime.UtcNow.Subtract(boxStopwatch.LastTimeStartUtcValue);
            }

            boxStopwatch.StopwatchValue = boxStopwatch.LastTimeValue + boxStopwatch.LastStopwatchValueBeforePause;

            return boxStopwatch;
        }

        public static int IncreaseStopwatchValue(SpecifierInfo specifier, int value)
        {
            switch (specifier.Type)
            {
                case SpecifierType.ms:
                case SpecifierType.mss:
                    {
                        if (value < 999)
                        {
                            value++;
                        }
                        else
                        {
                            value = 1;
                        }
                        break;
                    }

                case SpecifierType.s:
                case SpecifierType.ss:
                    {
                        if (value < 59)
                        {
                            value++;
                        }
                        else
                        {
                            value = 1;
                        }
                        break;
                    }

                case SpecifierType.m:
                case SpecifierType.mm:
                    {
                        if (value < 59)
                        {
                            value++;
                        }
                        else
                        {
                            value = 1;
                        }
                        break;
                    }

                case SpecifierType.h:
                case SpecifierType.hh:
                case SpecifierType.H:
                case SpecifierType.HH:
                    {
                        if (value > 0)
                        {
                            value++;
                        }
                        else
                        {
                            value = 0;

                        }
                        break;
                    }

                default:
                    {
                        value = -1;

                        break;
                    }
            }

            return value;
        }

        public static void IncreaseStopwatchValue_StepByStep(TimeSpan value, bool showMillisecond)
        {
            int milliseconds = value.Milliseconds;
            int seconds = value.Seconds;
            int minutes = value.Minutes;
            int hours = value.Hours;
            int days = value.Days;

            if (showMillisecond)
            {
                /// Define milliseconds if(milliseconds == 999)
                if (milliseconds == 999 && seconds < 59)
                {
                    milliseconds = 0;
                    seconds++;
                }
                else if (milliseconds == 999 && seconds == 59 && minutes < 59)
                {
                    seconds = 0; milliseconds = 0;
                    minutes++;
                }
                else if (milliseconds == 999 && seconds == 59 && minutes == 59 && hours < 23)
                {
                    minutes = 0; seconds = 0; milliseconds = 0;
                    hours++;
                }
                else if (milliseconds == 999 && seconds == 59 && minutes == 59 && hours == 23 && days > -1)
                {
                    hours = 0; minutes = 0; seconds = 0; milliseconds = 0;
                    days++;
                }
            }

            /// Define seconds if(seconds == 59)
            if (seconds == 59 && minutes < 59)
            {
                seconds = 0;
                minutes++;
            }
            else if (seconds == 59 && minutes == 59 && hours < 23)
            {
                minutes = 0; seconds = 0;
                hours++;
            }
            else if (seconds == 59 && minutes == 59 && hours == 23 && days > -1)
            {
                hours = 0; minutes = 0; seconds = 0;
                days++;
            }

            /// Define minutes if(minutes == 59)
            if (minutes == 59 && hours < 23)
            {
                minutes = 0;
                hours++;
            }
            else if (minutes == 59 && hours == 23 && days > -1)
            {
                hours = 0; minutes = 0;
                days++;
            }

            /// Define hours if(hours == 23)
            if (hours == 23 && days > -1)
            {
                hours = 0;
                days++;
            }
        }

        #endregion
    }
}
