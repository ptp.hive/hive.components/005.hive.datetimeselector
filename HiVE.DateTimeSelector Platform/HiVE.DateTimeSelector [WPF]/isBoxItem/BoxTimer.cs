﻿using System;

namespace HiVE.DateTimeSelector
{
    public class BoxTimer
    {
        public TimeSpan LastStopwatchValueBeforePause { get; set; }
        public DateTime LastTimeStartUtcValue { get; set; }
        public TimeSpan LastTimeValue { get; set; }
        public TimeSpan IntervalTick { get; set; }
        public int NextStepTick { get; set; }
        public TimeSpan StopwatchValue { get; set; }
        public TimeSpan MaxTimeValue { get; set; }
        public TimeSpan TimerValue { get; set; }

        /// <summary>
        /// Box Timer Details
        /// </summary>
        public BoxTimer() { }

        /// <summary>
        /// Box Timer Details
        /// </summary>
        /// <param name="lastStopwatchValueBeforePause"></param>
        /// <param name="lastTimeStartUtcValue"></param>
        /// <param name="lastTimeValue"></param>
        /// <param name="intervalTick"></param>
        /// <param name="nextStepTick"></param>
        /// <param name="stopwatchValue"></param>
        /// <param name="maxTimeValue"></param>
        /// <param name="timerValue"></param>
        public BoxTimer(
            TimeSpan lastStopwatchValueBeforePause,
            DateTime lastTimeStartUtcValue,
            TimeSpan lastTimeValue,
            TimeSpan intervalTick,
            int nextStepTick,
            TimeSpan stopwatchValue,
            TimeSpan maxTimeValue,
            TimeSpan timerValue)
        {
            this.LastStopwatchValueBeforePause = lastStopwatchValueBeforePause;
            this.LastTimeStartUtcValue = lastTimeStartUtcValue;
            this.LastTimeValue = lastTimeValue;
            this.IntervalTick = intervalTick;
            this.NextStepTick = nextStepTick;
            this.MaxTimeValue = maxTimeValue;
            this.StopwatchValue = stopwatchValue;
            this.TimerValue = timerValue;
        }
    }
}
