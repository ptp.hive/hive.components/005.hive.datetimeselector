﻿using HiVE.DateTimeSelector.Models;

namespace HiVE.DateTimeSelector
{
    public class BoxBasicTicks
    {
        #region Methods

        /// <summary>
        /// Stopwatch Tick
        /// CountUp Time
        /// </summary>
        /// <param name="boxStopwatch"></param>
        /// <returns></returns>
        public static BoxStopwatch Stopwatch_Tick(BoxStopwatch boxStopwatch)
        {
            return Stopwatch.Stopwatch_Tick(boxStopwatch);
        }

        /// <summary>
        /// Timer Tick
        /// CountDown Time
        /// </summary>
        /// <param name="boxTimer"></param>
        /// <returns></returns>
        public static BoxTimer Timer_Tick(BoxTimer boxTimer)
        {
            return Timer.Timer_Tick(boxTimer);
        }

        #endregion
    }
}
