﻿namespace HiVE.DateTimeSelector
{
    internal static class BoxSpecialMethods
    {
        #region Fields BasicMethods

        public static int threshold_Milliseconds
        {
            get { return (20); }
        }

        #endregion

        #region Specifier info

        public enum CustomValueType
        {
            None = 0,
            Numeral,
            Items,
            Static,
            StaticItems,
            StringLiteral,
        }

        public enum SpecifierType
        {
            ms,
            mss,
            s,
            ss,
            m,
            mm,
            h,
            hh,
            H,
            HH,
            t,
            tt,
            d,
            dd,
            ddd,
            dddd,
            M,
            MM,
            MMM,
            MMMM,
            y,
            yy,
            yyy,
            yyyy,
            g,
            DateSeparator,
            TimeSeparator,
            StringLiteral,
        }

        public struct SpecifierInfo
        {
            public SpecifierInfo(
                SpecifierType type,
                CustomValueType valueType,
                string symbol,
                int max,
                bool matchesExtraSymbols)
                : this()
            {
                MatchesExtraSymbols = matchesExtraSymbols;
                MaxLength = max;
                Symbol = symbol;
                Type = type;
                ValueType = valueType;
            }
            public bool MatchesExtraSymbols
            {
                get;
                set;
            }
            public int MaxLength
            {
                get;
                set;
            }
            public string Symbol
            {
                get;
                set;
            }
            public SpecifierType Type
            {
                get;
                set;
            }
            public CustomValueType ValueType
            {
                get;
                set;
            }
            public static SpecifierInfo CreateStringLiteralSpecifier(string text)
            {
                return new SpecifierInfo(SpecifierType.StringLiteral, CustomValueType.StringLiteral, text, -1, false);

            }
            public override string ToString()
            {
                if (this.Symbol != null)
                {
                    return this.Symbol.ToString();
                }
                return base.ToString();
            }
        }

        #endregion
    }
}
