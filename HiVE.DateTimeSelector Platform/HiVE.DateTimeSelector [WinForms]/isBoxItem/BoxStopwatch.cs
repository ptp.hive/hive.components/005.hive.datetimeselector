﻿using System;

namespace HiVE.DateTimeSelector
{
    public class BoxStopwatch
    {
        public TimeSpan LastStopwatchValueBeforePause { get; set; }
        public DateTime LastTimeStartUtcValue { get; set; }
        public TimeSpan LastTimeValue { get; set; }
        public TimeSpan IntervalTick { get; set; }
        public int NextStepTick { get; set; }
        public TimeSpan StopwatchValue { get; set; }

        /// <summary>
        /// Box Stopwatch Details
        /// </summary>
        public BoxStopwatch() { }

        /// <summary>
        /// Box Stopwatch Details
        /// </summary>
        /// <param name="lastStopwatchValueBeforePause"></param>
        /// <param name="lastTimeStartUtcValue"></param>
        /// <param name="lastTimeValue"></param>
        /// <param name="intervalTick"></param>
        /// <param name="nextStepTick"></param>
        /// <param name="stopwatchValue"></param>
        public BoxStopwatch(
            TimeSpan lastStopwatchValueBeforePause,
            DateTime lastTimeStartUtcValue,
            TimeSpan lastTimeValue,
            TimeSpan intervalTick,
            int nextStepTick,
            TimeSpan stopwatchValue)
        {
            this.LastStopwatchValueBeforePause = lastStopwatchValueBeforePause;
            this.LastTimeStartUtcValue = lastTimeStartUtcValue;
            this.LastTimeValue = lastTimeValue;
            this.IntervalTick = intervalTick;
            this.NextStepTick = nextStepTick;
            this.StopwatchValue = stopwatchValue;
        }
    }
}
