﻿namespace HiVE.DateTimeSelector_Demo_WinForms.isUserControl
{
    partial class ucDateTimeSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanelMain = new System.Windows.Forms.FlowLayoutPanel();
            this.labelWinForms_DateTimePicker = new System.Windows.Forms.Label();
            this.checkBoxRightToLeftLayout = new System.Windows.Forms.CheckBox();
            this.dateTimePickerWinForms_DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.labelWPF_DatePicker = new System.Windows.Forms.Label();
            this.checkBoxFlowDirection = new System.Windows.Forms.CheckBox();
            this.elementHostWPF_DatePicker = new System.Windows.Forms.Integration.ElementHost();
            this.wpfUserControl_DatePicker1 = new HiVE.DateTimeSelector_Demo_WinForms.isUserControl.WPFUserControl_DatePicker();
            this.labelDateTimeSelector = new System.Windows.Forms.Label();
            this.checkBoxUsePersianFormat = new System.Windows.Forms.CheckBox();
            this.dateTimeSelectorTest = new HiVE.DateTimeSelector.DateTimeSelector();
            this.flowLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanelMain
            // 
            this.flowLayoutPanelMain.AutoScroll = true;
            this.flowLayoutPanelMain.AutoSize = true;
            this.flowLayoutPanelMain.Controls.Add(this.labelWinForms_DateTimePicker);
            this.flowLayoutPanelMain.Controls.Add(this.checkBoxRightToLeftLayout);
            this.flowLayoutPanelMain.Controls.Add(this.dateTimePickerWinForms_DateTimePicker);
            this.flowLayoutPanelMain.Controls.Add(this.labelWPF_DatePicker);
            this.flowLayoutPanelMain.Controls.Add(this.checkBoxFlowDirection);
            this.flowLayoutPanelMain.Controls.Add(this.elementHostWPF_DatePicker);
            this.flowLayoutPanelMain.Controls.Add(this.labelDateTimeSelector);
            this.flowLayoutPanelMain.Controls.Add(this.checkBoxUsePersianFormat);
            this.flowLayoutPanelMain.Controls.Add(this.dateTimeSelectorTest);
            this.flowLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelMain.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelMain.Name = "flowLayoutPanelMain";
            this.flowLayoutPanelMain.Size = new System.Drawing.Size(260, 340);
            this.flowLayoutPanelMain.TabIndex = 0;
            this.flowLayoutPanelMain.WrapContents = false;
            // 
            // labelWinForms_DateTimePicker
            // 
            this.labelWinForms_DateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWinForms_DateTimePicker.AutoSize = true;
            this.labelWinForms_DateTimePicker.Location = new System.Drawing.Point(5, 5);
            this.labelWinForms_DateTimePicker.Margin = new System.Windows.Forms.Padding(5);
            this.labelWinForms_DateTimePicker.Name = "labelWinForms_DateTimePicker";
            this.labelWinForms_DateTimePicker.Size = new System.Drawing.Size(161, 13);
            this.labelWinForms_DateTimePicker.TabIndex = 0;
            this.labelWinForms_DateTimePicker.Text = "WinForms:DateTimePicker!";
            // 
            // checkBoxRightToLeftLayout
            // 
            this.checkBoxRightToLeftLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxRightToLeftLayout.AutoSize = true;
            this.checkBoxRightToLeftLayout.Location = new System.Drawing.Point(5, 28);
            this.checkBoxRightToLeftLayout.Margin = new System.Windows.Forms.Padding(5);
            this.checkBoxRightToLeftLayout.Name = "checkBoxRightToLeftLayout";
            this.checkBoxRightToLeftLayout.Size = new System.Drawing.Size(161, 17);
            this.checkBoxRightToLeftLayout.TabIndex = 1;
            this.checkBoxRightToLeftLayout.Text = "RightToLeftLayout";
            this.checkBoxRightToLeftLayout.UseVisualStyleBackColor = true;
            this.checkBoxRightToLeftLayout.CheckedChanged += new System.EventHandler(this.checkBoxRightToLeftLayout_CheckedChanged);
            // 
            // dateTimePickerWinForms_DateTimePicker
            // 
            this.dateTimePickerWinForms_DateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerWinForms_DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerWinForms_DateTimePicker.Location = new System.Drawing.Point(5, 55);
            this.dateTimePickerWinForms_DateTimePicker.Margin = new System.Windows.Forms.Padding(5);
            this.dateTimePickerWinForms_DateTimePicker.Name = "dateTimePickerWinForms_DateTimePicker";
            this.dateTimePickerWinForms_DateTimePicker.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dateTimePickerWinForms_DateTimePicker.Size = new System.Drawing.Size(161, 20);
            this.dateTimePickerWinForms_DateTimePicker.TabIndex = 2;
            // 
            // labelWPF_DatePicker
            // 
            this.labelWPF_DatePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWPF_DatePicker.AutoSize = true;
            this.labelWPF_DatePicker.Location = new System.Drawing.Point(5, 105);
            this.labelWPF_DatePicker.Margin = new System.Windows.Forms.Padding(5, 25, 5, 5);
            this.labelWPF_DatePicker.Name = "labelWPF_DatePicker";
            this.labelWPF_DatePicker.Size = new System.Drawing.Size(161, 13);
            this.labelWPF_DatePicker.TabIndex = 3;
            this.labelWPF_DatePicker.Text = "WPF:DatePicker!";
            // 
            // checkBoxFlowDirection
            // 
            this.checkBoxFlowDirection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxFlowDirection.AutoSize = true;
            this.checkBoxFlowDirection.Location = new System.Drawing.Point(5, 128);
            this.checkBoxFlowDirection.Margin = new System.Windows.Forms.Padding(5);
            this.checkBoxFlowDirection.Name = "checkBoxFlowDirection";
            this.checkBoxFlowDirection.Size = new System.Drawing.Size(161, 17);
            this.checkBoxFlowDirection.TabIndex = 4;
            this.checkBoxFlowDirection.Text = "FlowDirection IsLeftToRight";
            this.checkBoxFlowDirection.UseVisualStyleBackColor = true;
            this.checkBoxFlowDirection.CheckedChanged += new System.EventHandler(this.checkBoxFlowDirection_CheckedChanged);
            // 
            // elementHostWPF_DatePicker
            // 
            this.elementHostWPF_DatePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elementHostWPF_DatePicker.AutoSize = true;
            this.elementHostWPF_DatePicker.Location = new System.Drawing.Point(5, 155);
            this.elementHostWPF_DatePicker.Margin = new System.Windows.Forms.Padding(5);
            this.elementHostWPF_DatePicker.Name = "elementHostWPF_DatePicker";
            this.elementHostWPF_DatePicker.Size = new System.Drawing.Size(161, 24);
            this.elementHostWPF_DatePicker.TabIndex = 5;
            this.elementHostWPF_DatePicker.Text = "elementHostWPF_DatePicker";
            this.elementHostWPF_DatePicker.Child = this.wpfUserControl_DatePicker1;
            // 
            // labelDateTimeSelector
            // 
            this.labelDateTimeSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDateTimeSelector.AutoSize = true;
            this.labelDateTimeSelector.Location = new System.Drawing.Point(5, 209);
            this.labelDateTimeSelector.Margin = new System.Windows.Forms.Padding(5, 25, 5, 5);
            this.labelDateTimeSelector.Name = "labelDateTimeSelector";
            this.labelDateTimeSelector.Size = new System.Drawing.Size(161, 13);
            this.labelDateTimeSelector.TabIndex = 6;
            this.labelDateTimeSelector.Text = "HiVE.DateTimeSelector!";
            // 
            // checkBoxUsePersianFormat
            // 
            this.checkBoxUsePersianFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxUsePersianFormat.AutoSize = true;
            this.checkBoxUsePersianFormat.Location = new System.Drawing.Point(5, 232);
            this.checkBoxUsePersianFormat.Margin = new System.Windows.Forms.Padding(5);
            this.checkBoxUsePersianFormat.Name = "checkBoxUsePersianFormat";
            this.checkBoxUsePersianFormat.Size = new System.Drawing.Size(161, 17);
            this.checkBoxUsePersianFormat.TabIndex = 7;
            this.checkBoxUsePersianFormat.Text = "UsePersianFormat";
            this.checkBoxUsePersianFormat.UseVisualStyleBackColor = true;
            this.checkBoxUsePersianFormat.CheckedChanged += new System.EventHandler(this.checkBoxUsePersianFormat_CheckedChanged);
            // 
            // dateTimeSelectorTest
            // 
            this.dateTimeSelectorTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimeSelectorTest.CustomFormat = "yyyy/MM/dd";
            this.dateTimeSelectorTest.Format = HiVE.DateTimeSelector.UI.DateTimeSelectorFormat.Custom;
            this.dateTimeSelectorTest.Location = new System.Drawing.Point(5, 259);
            this.dateTimeSelectorTest.Margin = new System.Windows.Forms.Padding(5);
            this.dateTimeSelectorTest.Name = "dateTimeSelectorTest";
            this.dateTimeSelectorTest.Size = new System.Drawing.Size(161, 21);
            this.dateTimeSelectorTest.TabIndex = 8;
            // 
            // ucDateTimeSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanelMain);
            this.Name = "ucDateTimeSelector";
            this.Size = new System.Drawing.Size(260, 340);
            this.Load += new System.EventHandler(this.DateTimeSelector_Load);
            this.flowLayoutPanelMain.ResumeLayout(false);
            this.flowLayoutPanelMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelMain;
        private System.Windows.Forms.Label labelWinForms_DateTimePicker;
        private System.Windows.Forms.CheckBox checkBoxRightToLeftLayout;
        private System.Windows.Forms.DateTimePicker dateTimePickerWinForms_DateTimePicker;
        private System.Windows.Forms.Label labelWPF_DatePicker;
        private System.Windows.Forms.CheckBox checkBoxFlowDirection;
        private System.Windows.Forms.Integration.ElementHost elementHostWPF_DatePicker;
        private WPFUserControl_DatePicker wpfUserControl_DatePicker1;
        private System.Windows.Forms.Label labelDateTimeSelector;
        private System.Windows.Forms.CheckBox checkBoxUsePersianFormat;
        private DateTimeSelector.DateTimeSelector dateTimeSelectorTest;
    }
}
