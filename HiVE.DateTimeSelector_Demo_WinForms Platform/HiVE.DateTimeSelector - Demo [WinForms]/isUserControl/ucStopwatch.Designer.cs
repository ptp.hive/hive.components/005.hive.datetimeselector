﻿namespace HiVE.DateTimeSelector_Demo_WinForms.isUserControl
{
    partial class ucStopwatch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.listBoxStopwatch_LogLap = new System.Windows.Forms.ListBox();
            this.flowLayoutPanelMain = new System.Windows.Forms.FlowLayoutPanel();
            this.checkBoxMilliseconds = new System.Windows.Forms.CheckBox();
            this.btnStopwatch_StartStop = new System.Windows.Forms.Button();
            this.btnStopwatch_Reset_Lap = new System.Windows.Forms.Button();
            this.lblStopwatch_Show = new System.Windows.Forms.Label();
            this.timer_Stopwatch = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanelMain.SuspendLayout();
            this.flowLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 166F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.listBoxStopwatch_LogLap, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanelMain, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.lblStopwatch_Show, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(560, 250);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // listBoxStopwatch_LogLap
            // 
            this.listBoxStopwatch_LogLap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxStopwatch_LogLap.FormattingEnabled = true;
            this.listBoxStopwatch_LogLap.Location = new System.Drawing.Point(176, 10);
            this.listBoxStopwatch_LogLap.Margin = new System.Windows.Forms.Padding(10);
            this.listBoxStopwatch_LogLap.Name = "listBoxStopwatch_LogLap";
            this.tableLayoutPanelMain.SetRowSpan(this.listBoxStopwatch_LogLap, 2);
            this.listBoxStopwatch_LogLap.Size = new System.Drawing.Size(374, 230);
            this.listBoxStopwatch_LogLap.TabIndex = 27;
            // 
            // flowLayoutPanelMain
            // 
            this.flowLayoutPanelMain.AutoSize = true;
            this.flowLayoutPanelMain.Controls.Add(this.checkBoxMilliseconds);
            this.flowLayoutPanelMain.Controls.Add(this.btnStopwatch_StartStop);
            this.flowLayoutPanelMain.Controls.Add(this.btnStopwatch_Reset_Lap);
            this.flowLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelMain.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelMain.Location = new System.Drawing.Point(5, 25);
            this.flowLayoutPanelMain.Margin = new System.Windows.Forms.Padding(5, 25, 5, 5);
            this.flowLayoutPanelMain.Name = "flowLayoutPanelMain";
            this.flowLayoutPanelMain.Padding = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanelMain.Size = new System.Drawing.Size(156, 161);
            this.flowLayoutPanelMain.TabIndex = 0;
            this.flowLayoutPanelMain.WrapContents = false;
            // 
            // checkBoxMilliseconds
            // 
            this.checkBoxMilliseconds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMilliseconds.AutoSize = true;
            this.checkBoxMilliseconds.Location = new System.Drawing.Point(8, 8);
            this.checkBoxMilliseconds.Name = "checkBoxMilliseconds";
            this.checkBoxMilliseconds.Size = new System.Drawing.Size(83, 17);
            this.checkBoxMilliseconds.TabIndex = 24;
            this.checkBoxMilliseconds.Text = "Milliseconds";
            this.checkBoxMilliseconds.UseVisualStyleBackColor = true;
            this.checkBoxMilliseconds.CheckedChanged += new System.EventHandler(this.checkBoxMilliseconds_CheckedChanged);
            // 
            // btnStopwatch_StartStop
            // 
            this.btnStopwatch_StartStop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopwatch_StartStop.Location = new System.Drawing.Point(8, 31);
            this.btnStopwatch_StartStop.Name = "btnStopwatch_StartStop";
            this.btnStopwatch_StartStop.Size = new System.Drawing.Size(83, 23);
            this.btnStopwatch_StartStop.TabIndex = 25;
            this.btnStopwatch_StartStop.Text = "Start / Stop";
            this.btnStopwatch_StartStop.UseVisualStyleBackColor = true;
            this.btnStopwatch_StartStop.Click += new System.EventHandler(this.btnStopwatch_StartStop_Click);
            // 
            // btnStopwatch_Reset_Lap
            // 
            this.btnStopwatch_Reset_Lap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopwatch_Reset_Lap.Location = new System.Drawing.Point(8, 60);
            this.btnStopwatch_Reset_Lap.Name = "btnStopwatch_Reset_Lap";
            this.btnStopwatch_Reset_Lap.Size = new System.Drawing.Size(83, 23);
            this.btnStopwatch_Reset_Lap.TabIndex = 26;
            this.btnStopwatch_Reset_Lap.Text = "Reset OR Lap";
            this.btnStopwatch_Reset_Lap.UseVisualStyleBackColor = true;
            this.btnStopwatch_Reset_Lap.Click += new System.EventHandler(this.btnStopwatch_Reset_Lap_Click);
            // 
            // lblStopwatch_Show
            // 
            this.lblStopwatch_Show.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStopwatch_Show.AutoSize = true;
            this.lblStopwatch_Show.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStopwatch_Show.ForeColor = System.Drawing.Color.Maroon;
            this.lblStopwatch_Show.Location = new System.Drawing.Point(5, 206);
            this.lblStopwatch_Show.Margin = new System.Windows.Forms.Padding(5, 15, 5, 15);
            this.lblStopwatch_Show.Name = "lblStopwatch_Show";
            this.lblStopwatch_Show.Padding = new System.Windows.Forms.Padding(5);
            this.lblStopwatch_Show.Size = new System.Drawing.Size(156, 29);
            this.lblStopwatch_Show.TabIndex = 25;
            this.lblStopwatch_Show.Text = "00:00:00.000";
            // 
            // timer_Stopwatch
            // 
            this.timer_Stopwatch.Tick += new System.EventHandler(this.timer_Stopwatch_Tick);
            // 
            // ucStopwatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "ucStopwatch";
            this.Size = new System.Drawing.Size(560, 250);
            this.Load += new System.EventHandler(this.Stopwatch_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.flowLayoutPanelMain.ResumeLayout(false);
            this.flowLayoutPanelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelMain;
        private System.Windows.Forms.Label lblStopwatch_Show;
        private System.Windows.Forms.CheckBox checkBoxMilliseconds;
        private System.Windows.Forms.Button btnStopwatch_StartStop;
        private System.Windows.Forms.Button btnStopwatch_Reset_Lap;
        private System.Windows.Forms.ListBox listBoxStopwatch_LogLap;
        private System.Windows.Forms.Timer timer_Stopwatch;
    }
}
