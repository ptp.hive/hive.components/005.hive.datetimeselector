﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HiVE.BasicModels.isBoxItem;
using static HiVE.BasicModels.isClass.TryCatch;
using HiVE.BasicModels.isClass;

namespace HiVE.DateTimeSelector_Demo_WinForms.isUserControl
{
    internal partial class ucDateTimeSelector : UserControl
    {
        public ucDateTimeSelector()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DateTimeSelector_Demo_WinForms;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl.DateTimeSelector).ToString();

        private TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl_DateTimeSelector TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        private void RefreshDateTimeSelector()
        {
            try
            {
                dateTimePickerWinForms_DateTimePicker.Value = DateTime.Now;
                dateTimeSelectorTest.Value = DateTime.Now;

                dateTimePickerWinForms_DateTimePicker.RightToLeftLayout = checkBoxRightToLeftLayout.Checked;
                dateTimeSelectorTest.UsePersianFormat = checkBoxUsePersianFormat.Checked;
            }
            catch { }
        }

        private void checkBoxRightToLeftLayout_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                dateTimePickerWinForms_DateTimePicker.RightToLeftLayout = checkBoxRightToLeftLayout.Checked;
            }
            catch { }
        }

        private void checkBoxFlowDirection_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch { }
        }

        private void checkBoxUsePersianFormat_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                dateTimeSelectorTest.UsePersianFormat = checkBoxUsePersianFormat.Checked;
            }
            catch { }
        }

        private void DateTimeSelector_Load(object sender, EventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl_DateTimeSelector.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                RefreshDateTimeSelector();
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
