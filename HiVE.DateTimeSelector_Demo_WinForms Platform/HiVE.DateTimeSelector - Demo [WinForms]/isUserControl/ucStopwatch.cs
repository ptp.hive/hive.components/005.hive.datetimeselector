﻿using System;
using System.Windows.Forms;
using HiVE.DateTimeSelector;
using System.Threading;
using HiVE.BasicModels.isBoxItem;
using static HiVE.BasicModels.isClass.TryCatch;
using HiVE.BasicModels.isClass;

namespace HiVE.DateTimeSelector_Demo_WinForms.isUserControl
{
    internal partial class ucStopwatch : UserControl
    {
        public ucStopwatch()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DateTimeSelector_Demo_WinForms;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl.Stopwatch).ToString();

        private TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl_Stopwatch TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        #region Fields

        bool status_Stopwatch_IsReset = false;
        TimeSpan timeSpanIntervalTick = new TimeSpan();
        private BoxStopwatch stopwatchStatus = new BoxStopwatch();

        #endregion

        #region MultiThreaded

        private Thread trTimer_Stopwatch;
        private ThreadStart thrsTimer_Stopwatch;

        #endregion

        #region Stopwatch Status

        private void timer_Stopwatch_Reset()
        {
            try
            {
                timeSpanIntervalTick = new TimeSpan(0, 0, 0, 0, timer_Stopwatch.Interval);

                stopwatchStatus = new BoxStopwatch(
                    new TimeSpan(0),
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    timeSpanIntervalTick,
                    1,
                    new TimeSpan(0));

                Invoke(new MethodInvoker(delegate ()
                {
                    lblStopwatch_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        stopwatchStatus.StopwatchValue.Hours,
                        stopwatchStatus.StopwatchValue.Minutes,
                        stopwatchStatus.StopwatchValue.Seconds,
                        stopwatchStatus.StopwatchValue.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Stopwatch_Start()
        {
            try
            {
                if (status_Stopwatch_IsReset)
                {
                    timer_Stopwatch_Reset();
                    status_Stopwatch_IsReset = false;
                }

                timeSpanIntervalTick = new TimeSpan(0, 0, 0, 0, timer_Stopwatch.Interval);

                stopwatchStatus = new BoxStopwatch(
                    stopwatchStatus.LastStopwatchValueBeforePause,
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    timeSpanIntervalTick,
                    1,
                    stopwatchStatus.StopwatchValue);

                timer_Stopwatch.Enabled = true;
            }
            catch { }
        }

        private void timer_Stopwatch_Counter()
        {
            try
            {
                stopwatchStatus =
                    BoxBasicTicks.Stopwatch_Tick(stopwatchStatus);

                Invoke(new MethodInvoker(delegate ()
                {
                    lblStopwatch_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        stopwatchStatus.StopwatchValue.Hours,
                        stopwatchStatus.StopwatchValue.Minutes,
                        stopwatchStatus.StopwatchValue.Seconds,
                        stopwatchStatus.StopwatchValue.Milliseconds);
                }));

                //Invoke(new MethodInvoker(delegate () { progressBarStatus.Value += 1; }));
            }
            catch { }
        }

        private void timer_Stopwatch_Tick(object sender, EventArgs e)
        {
            try
            {
                thrsTimer_Stopwatch = new ThreadStart(timer_Stopwatch_Counter);
                trTimer_Stopwatch = new Thread(thrsTimer_Stopwatch);
                trTimer_Stopwatch.Priority = ThreadPriority.AboveNormal;
                //
                trTimer_Stopwatch.Start();
            }
            catch { }
        }

        private void timer_Stopwatch_Stop()
        {
            try
            {
                timer_Stopwatch.Enabled = false;

                stopwatchStatus =
                    BoxBasicTicks.Stopwatch_Tick(stopwatchStatus);

                stopwatchStatus.LastStopwatchValueBeforePause = stopwatchStatus.StopwatchValue;

                Invoke(new MethodInvoker(delegate ()
                {
                    lblStopwatch_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        stopwatchStatus.StopwatchValue.Hours,
                        stopwatchStatus.StopwatchValue.Minutes,
                        stopwatchStatus.StopwatchValue.Seconds,
                        stopwatchStatus.StopwatchValue.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Stopwatch_Lap()
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    string strStatusProcess = string.Empty;
                    if (timer_Stopwatch.Enabled)
                    {
                        strStatusProcess = "Started";
                    }
                    else
                    {
                        strStatusProcess = "Stoped";
                    }

                    listBoxStopwatch_LogLap.Items.Add(
                        string.Format(
                            "Process {0}, In Time [{1}]! Status is => [{2}:{3}:{4}.{5}]!",
                            strStatusProcess,
                            DateTime.Now,
                            stopwatchStatus.StopwatchValue.Hours,
                            stopwatchStatus.StopwatchValue.Minutes,
                            stopwatchStatus.StopwatchValue.Seconds,
                            stopwatchStatus.StopwatchValue.Milliseconds));
                }));
            }
            catch { }
        }

        #endregion

        #region Stopwatch Functions

        private void checkBoxMilliseconds_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    if (checkBoxMilliseconds.Checked)
                    {
                        timer_Stopwatch.Interval = 1;
                    }
                    else
                    {
                        timer_Stopwatch.Interval = 1000;
                    }
                }));
            }
            catch { }
        }

        private bool timer_Stopwatch_EnabledChanged(bool isEnabled)
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    checkBoxMilliseconds.Enabled = !isEnabled;
                }));

                if (isEnabled)
                {
                    Invoke(new MethodInvoker(delegate ()
                    {
                        btnStopwatch_Reset_Lap.Text = "Lap";
                    }));
                }
                else
                {
                    Invoke(new MethodInvoker(delegate ()
                    {
                        btnStopwatch_Reset_Lap.Text = "Reset";
                    }));
                }
            }
            catch { return false; }

            return true;
        }

        private void btnStopwatch_StartStop_Click(object sender, EventArgs e)
        {
            try
            {
                if (!timer_Stopwatch.Enabled)
                {
                    timer_Stopwatch_Start();

                    timer_Stopwatch_EnabledChanged(timer_Stopwatch.Enabled);
                    timer_Stopwatch_Lap();
                }
                else
                {
                    timer_Stopwatch_Stop();

                    timer_Stopwatch_EnabledChanged(timer_Stopwatch.Enabled);
                    timer_Stopwatch_Lap();
                }
            }
            catch { }
        }

        private void btnStopwatch_Reset_Lap_Click(object sender, EventArgs e)
        {
            try
            {
                if (timer_Stopwatch.Enabled)
                {
                    timer_Stopwatch_Lap();
                }
                else
                {
                    timer_Stopwatch_Reset();
                }
            }
            catch { }
        }

        #endregion

        private void RefreshStopwatch()
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    if (checkBoxMilliseconds.Checked)
                    {
                        timer_Stopwatch.Interval = 1;
                    }
                    else
                    {
                        timer_Stopwatch.Interval = 1000;
                    }
                }));

                timer_Stopwatch_EnabledChanged(timer_Stopwatch.Enabled);
                status_Stopwatch_IsReset = true;
            }
            catch { }
        }

        private void Stopwatch_Load(object sender, EventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl_Stopwatch.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                RefreshStopwatch();
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
