﻿using System;
using System.Windows.Forms;
using HiVE.DateTimeSelector;
using System.Threading;
using HiVE.BasicModels.isBoxItem;
using static HiVE.BasicModels.isClass.TryCatch;
using HiVE.BasicModels.isClass;

namespace HiVE.DateTimeSelector_Demo_WinForms.isUserControl
{
    internal partial class ucTimer : UserControl
    {
        public ucTimer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DateTimeSelector_Demo_WinForms;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl.DateTimeSelector).ToString();

        private TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl_DateTimeSelector TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        #region Fields

        bool status_Timer_IsReset = false;
        TimeSpan timeSpanIntervalTick = new TimeSpan();
        private BoxTimer timerStatus = new BoxTimer();
        TimeSpan maxTimeValue_Status = new TimeSpan();

        #endregion

        #region MultiThreaded

        private Thread trTimer_Timer;
        private ThreadStart thrsTimer_Timer;

        #endregion

        #region Timer Status

        private void timer_Timer_Reset()
        {
            try
            {
                maxTimeValue_Status =
                    new TimeSpan(
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(0, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(3, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(6, 2))
                        );

                timeSpanIntervalTick = new TimeSpan(0, 0, 0, 0, timer_Timer.Interval);

                timerStatus = new BoxTimer(
                    new TimeSpan(0),
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    timeSpanIntervalTick,
                    1,
                    new TimeSpan(0),
                    new TimeSpan(0),
                    new TimeSpan(0));

                Invoke(new MethodInvoker(delegate ()
                {
                    lblTimer_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        maxTimeValue_Status.Hours,
                        maxTimeValue_Status.Minutes,
                        maxTimeValue_Status.Seconds,
                        maxTimeValue_Status.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Timer_Start()
        {
            try
            {
                if (status_Timer_IsReset)
                {
                    timer_Timer_Reset();
                    status_Timer_IsReset = false;
                }

                maxTimeValue_Status =
                    new TimeSpan(
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(0, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(3, 2)),
                        Int32.Parse(maskedtxtMaxTimeValue.Text.Substring(6, 2))
                        );

                timeSpanIntervalTick = new TimeSpan(0, 0, 0, 0, timer_Timer.Interval);

                timerStatus = new BoxTimer(
                    timerStatus.LastStopwatchValueBeforePause,
                    DateTime.UtcNow,
                    new TimeSpan(0),
                    timeSpanIntervalTick,
                    1,
                    timerStatus.StopwatchValue,
                    maxTimeValue_Status,
                    timerStatus.TimerValue);

                timer_Timer.Enabled = true;
            }
            catch { }
        }

        private void timer_Timer_Counter()
        {
            try
            {
                timerStatus =
                    BoxBasicTicks.Timer_Tick(timerStatus);

                Invoke(new MethodInvoker(delegate ()
                {
                    lblTimer_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        timerStatus.TimerValue.Hours,
                        timerStatus.TimerValue.Minutes,
                        timerStatus.TimerValue.Seconds,
                        timerStatus.TimerValue.Milliseconds);
                }));

                if (timerStatus.TimerValue.Ticks <= 0)
                {
                    timer_Timer_Stop();

                    timer_Timer_EnabledChanged(timer_Timer.Enabled);
                    timer_Timer_Lap();

                    btnTimer_StartStop.Enabled = false;
                }
            }
            catch { }
        }

        private void timer_Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                thrsTimer_Timer = new ThreadStart(timer_Timer_Counter);
                trTimer_Timer = new Thread(thrsTimer_Timer);
                trTimer_Timer.Priority = ThreadPriority.AboveNormal;
                //
                trTimer_Timer.Start();
            }
            catch { }
        }

        private void timer_Timer_Stop()
        {
            try
            {
                timer_Timer.Enabled = false;

                timerStatus =
                    BoxBasicTicks.Timer_Tick(timerStatus);

                timerStatus.LastStopwatchValueBeforePause = timerStatus.StopwatchValue;

                Invoke(new MethodInvoker(delegate ()
                {
                    lblTimer_Show.Text =
                    string.Format(
                        "{0}:{1}:{2}.{3}",
                        timerStatus.TimerValue.Hours,
                        timerStatus.TimerValue.Minutes,
                        timerStatus.TimerValue.Seconds,
                        timerStatus.TimerValue.Milliseconds);
                }));
            }
            catch { }
        }

        private void timer_Timer_Lap()
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    string strStatusProcess = string.Empty;
                    if (timer_Timer.Enabled)
                    {
                        strStatusProcess = "Started";
                    }
                    else
                    {
                        strStatusProcess = "Stoped";
                    }

                    listBoxTimer_LogLap.Items.Add(
                        string.Format(
                            "Process {0}, In Time [{1}]! Status is => [{2}:{3}:{4}.{5}]!",
                            strStatusProcess,
                            DateTime.Now,
                            timerStatus.TimerValue.Hours,
                            timerStatus.TimerValue.Minutes,
                            timerStatus.TimerValue.Seconds,
                            timerStatus.TimerValue.Milliseconds));
                }));
            }
            catch { }
        }

        #endregion

        #region Timer Functions

        private void checkBoxMilliseconds_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    if (checkBoxMilliseconds.Checked)
                    {
                        timer_Timer.Interval = 1;
                    }
                    else
                    {
                        timer_Timer.Interval = 1000;
                    }
                }));
            }
            catch { }
        }

        private bool timer_Timer_EnabledChanged(bool isEnabled)
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    checkBoxMilliseconds.Enabled = !isEnabled;
                    maskedtxtMaxTimeValue.Enabled = !isEnabled;
                }));

                if (isEnabled)
                {
                    Invoke(new MethodInvoker(delegate ()
                    {
                        btnTimer_Reset_Lap.Text = "Lap";
                    }));
                }
                else
                {
                    Invoke(new MethodInvoker(delegate ()
                    {
                        btnTimer_Reset_Lap.Text = "Reset";
                    }));
                }
            }
            catch { return false; }

            return true;
        }

        private void btnTimer_StartStop_Click(object sender, EventArgs e)
        {
            try
            {
                if (!timer_Timer.Enabled)
                {
                    timer_Timer_Start();

                    timer_Timer_EnabledChanged(timer_Timer.Enabled);
                    timer_Timer_Lap();
                }
                else
                {
                    timer_Timer_Stop();

                    timer_Timer_EnabledChanged(timer_Timer.Enabled);
                    timer_Timer_Lap();
                }
            }
            catch { }
        }

        private void btnTimer_Reset_Lap_Click(object sender, EventArgs e)
        {
            try
            {
                if (timer_Timer.Enabled)
                {
                    timer_Timer_Lap();
                }
                else
                {
                    timer_Timer_Reset();
                    btnTimer_StartStop.Enabled = true;
                }
            }
            catch { }
        }

        #endregion

        private void RefreshTimer()
        {
            try
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    if (checkBoxMilliseconds.Checked)
                    {
                        timer_Timer.Interval = 1;
                    }
                    else
                    {
                        timer_Timer.Interval = 1000;
                    }
                }));

                timer_Timer_EnabledChanged(timer_Timer.Enabled);
                status_Timer_IsReset = true;
            }
            catch { }
        }

        private void Timer_Load(object sender, EventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DateTimeSelector_Demo_WinForms_IsUserControl_Timer.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                RefreshTimer();
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
