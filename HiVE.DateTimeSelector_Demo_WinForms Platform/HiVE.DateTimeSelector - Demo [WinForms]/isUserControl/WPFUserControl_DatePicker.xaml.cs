﻿using System.Windows;
using System.Windows.Controls;

namespace HiVE.DateTimeSelector_Demo_WinForms.isUserControl
{
    /// <summary>
    /// Interaction logic for WPFUserControl_DatePicker.xaml
    /// </summary>
    internal partial class WPFUserControl_DatePicker : UserControl
    {
        public WPFUserControl_DatePicker()
        {
            InitializeComponent();
        }

        private void wPFUserControl_DatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                WPF_DatePicker.DisplayDate = System.DateTime.Now;
            }
            catch { }
        }
    }
}
