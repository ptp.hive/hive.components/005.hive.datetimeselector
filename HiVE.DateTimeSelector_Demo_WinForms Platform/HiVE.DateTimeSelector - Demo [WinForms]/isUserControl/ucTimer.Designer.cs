﻿namespace HiVE.DateTimeSelector_Demo_WinForms.isUserControl
{
    partial class ucTimer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.listBoxTimer_LogLap = new System.Windows.Forms.ListBox();
            this.flowLayoutPanelMain = new System.Windows.Forms.FlowLayoutPanel();
            this.maskedtxtMaxTimeValue = new System.Windows.Forms.MaskedTextBox();
            this.checkBoxMilliseconds = new System.Windows.Forms.CheckBox();
            this.btnTimer_StartStop = new System.Windows.Forms.Button();
            this.btnTimer_Reset_Lap = new System.Windows.Forms.Button();
            this.lblTimer_Show = new System.Windows.Forms.Label();
            this.timer_Timer = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanelMain.SuspendLayout();
            this.flowLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 166F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.listBoxTimer_LogLap, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanelMain, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.lblTimer_Show, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(560, 250);
            this.tableLayoutPanelMain.TabIndex = 1;
            // 
            // listBoxTimer_LogLap
            // 
            this.listBoxTimer_LogLap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxTimer_LogLap.FormattingEnabled = true;
            this.listBoxTimer_LogLap.Location = new System.Drawing.Point(176, 10);
            this.listBoxTimer_LogLap.Margin = new System.Windows.Forms.Padding(10);
            this.listBoxTimer_LogLap.Name = "listBoxTimer_LogLap";
            this.tableLayoutPanelMain.SetRowSpan(this.listBoxTimer_LogLap, 2);
            this.listBoxTimer_LogLap.Size = new System.Drawing.Size(374, 230);
            this.listBoxTimer_LogLap.TabIndex = 27;
            // 
            // flowLayoutPanelMain
            // 
            this.flowLayoutPanelMain.AutoSize = true;
            this.flowLayoutPanelMain.Controls.Add(this.maskedtxtMaxTimeValue);
            this.flowLayoutPanelMain.Controls.Add(this.checkBoxMilliseconds);
            this.flowLayoutPanelMain.Controls.Add(this.btnTimer_StartStop);
            this.flowLayoutPanelMain.Controls.Add(this.btnTimer_Reset_Lap);
            this.flowLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelMain.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelMain.Location = new System.Drawing.Point(5, 25);
            this.flowLayoutPanelMain.Margin = new System.Windows.Forms.Padding(5, 25, 5, 5);
            this.flowLayoutPanelMain.Name = "flowLayoutPanelMain";
            this.flowLayoutPanelMain.Padding = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanelMain.Size = new System.Drawing.Size(156, 161);
            this.flowLayoutPanelMain.TabIndex = 0;
            this.flowLayoutPanelMain.WrapContents = false;
            // 
            // maskedtxtMaxTimeValue
            // 
            this.maskedtxtMaxTimeValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maskedtxtMaxTimeValue.Location = new System.Drawing.Point(8, 8);
            this.maskedtxtMaxTimeValue.Mask = "00:00:00";
            this.maskedtxtMaxTimeValue.Name = "maskedtxtMaxTimeValue";
            this.maskedtxtMaxTimeValue.Size = new System.Drawing.Size(83, 20);
            this.maskedtxtMaxTimeValue.TabIndex = 34;
            this.maskedtxtMaxTimeValue.Text = "000000";
            this.maskedtxtMaxTimeValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxMilliseconds
            // 
            this.checkBoxMilliseconds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMilliseconds.AutoSize = true;
            this.checkBoxMilliseconds.Location = new System.Drawing.Point(8, 34);
            this.checkBoxMilliseconds.Name = "checkBoxMilliseconds";
            this.checkBoxMilliseconds.Size = new System.Drawing.Size(83, 17);
            this.checkBoxMilliseconds.TabIndex = 24;
            this.checkBoxMilliseconds.Text = "Milliseconds";
            this.checkBoxMilliseconds.UseVisualStyleBackColor = true;
            this.checkBoxMilliseconds.CheckedChanged += new System.EventHandler(this.checkBoxMilliseconds_CheckedChanged);
            // 
            // btnTimer_StartStop
            // 
            this.btnTimer_StartStop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTimer_StartStop.Location = new System.Drawing.Point(8, 57);
            this.btnTimer_StartStop.Name = "btnTimer_StartStop";
            this.btnTimer_StartStop.Size = new System.Drawing.Size(83, 23);
            this.btnTimer_StartStop.TabIndex = 25;
            this.btnTimer_StartStop.Text = "Start / Stop";
            this.btnTimer_StartStop.UseVisualStyleBackColor = true;
            this.btnTimer_StartStop.Click += new System.EventHandler(this.btnTimer_StartStop_Click);
            // 
            // btnTimer_Reset_Lap
            // 
            this.btnTimer_Reset_Lap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTimer_Reset_Lap.Location = new System.Drawing.Point(8, 86);
            this.btnTimer_Reset_Lap.Name = "btnTimer_Reset_Lap";
            this.btnTimer_Reset_Lap.Size = new System.Drawing.Size(83, 23);
            this.btnTimer_Reset_Lap.TabIndex = 26;
            this.btnTimer_Reset_Lap.Text = "Reset OR Lap";
            this.btnTimer_Reset_Lap.UseVisualStyleBackColor = true;
            this.btnTimer_Reset_Lap.Click += new System.EventHandler(this.btnTimer_Reset_Lap_Click);
            // 
            // lblTimer_Show
            // 
            this.lblTimer_Show.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTimer_Show.AutoSize = true;
            this.lblTimer_Show.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer_Show.ForeColor = System.Drawing.Color.Maroon;
            this.lblTimer_Show.Location = new System.Drawing.Point(5, 206);
            this.lblTimer_Show.Margin = new System.Windows.Forms.Padding(5, 15, 5, 15);
            this.lblTimer_Show.Name = "lblTimer_Show";
            this.lblTimer_Show.Padding = new System.Windows.Forms.Padding(5);
            this.lblTimer_Show.Size = new System.Drawing.Size(156, 29);
            this.lblTimer_Show.TabIndex = 25;
            this.lblTimer_Show.Text = "00:00:00.000";
            // 
            // timer_Timer
            // 
            this.timer_Timer.Tick += new System.EventHandler(this.timer_Timer_Tick);
            // 
            // ucTimer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "ucTimer";
            this.Size = new System.Drawing.Size(560, 250);
            this.Load += new System.EventHandler(this.Timer_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.flowLayoutPanelMain.ResumeLayout(false);
            this.flowLayoutPanelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.ListBox listBoxTimer_LogLap;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelMain;
        private System.Windows.Forms.CheckBox checkBoxMilliseconds;
        private System.Windows.Forms.Button btnTimer_StartStop;
        private System.Windows.Forms.Button btnTimer_Reset_Lap;
        private System.Windows.Forms.Label lblTimer_Show;
        private System.Windows.Forms.MaskedTextBox maskedtxtMaxTimeValue;
        private System.Windows.Forms.Timer timer_Timer;
    }
}
