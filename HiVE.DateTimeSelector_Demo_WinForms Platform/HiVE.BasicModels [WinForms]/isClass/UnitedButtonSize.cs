﻿using System.Windows;
using System.Windows.Controls;

namespace HiVE.BasicModels.isClass
{
    public static class UnitedButtonSize
    {
        /// <summary>
        /// Get United Buttons Size Group
        /// Size Max Button
        /// </summary>
        /// <param name="buttons"></param>
        /// <returns>United Buttons Size</returns>
        public static Button[] GetUnitedButtonSizeGroup(Button[] buttons)
        {
            Size sizeMaxButton = new Size(0, 0);

            try
            {
                if (buttons != null)
                {
                    foreach (Button btn in buttons)
                    {
                        if (btn != null)
                        {
                            if (btn.ActualWidth > sizeMaxButton.Width)
                            {
                                sizeMaxButton.Width = (double)btn.ActualWidth;
                            }
                            if (btn.ActualHeight > sizeMaxButton.Height)
                            {
                                sizeMaxButton.Height = (double)btn.ActualHeight;
                            }
                        }
                    }
                    foreach (Button btn in buttons)
                    {
                        if (btn != null)
                        {
                            btn.Width = sizeMaxButton.Width;
                            btn.Height = sizeMaxButton.Height;
                        }
                    }
                }
            }
            catch { return null; }

            return buttons;
        }

        /// <summary>
        /// Calculate Buttons Size
        /// United Sise
        /// </summary>
        /// <param name="buttonsContent"></param>
        /// <returns></returns>
        public static Size CalculateButtonsSize(string[] buttonsContent)
        {
            Size sizeMaxButton = new Size(0, 0);

            WrapPanel wrapPanel = new WrapPanel();

            try
            {
                Button buttonSample = new Button();

                for (int index = 0; index < buttonsContent.Length; index++)
                {
                    if (buttonsContent[index] != null)
                    {
                        buttonsContent[index] = buttonsContent[index].Trim();

                        if (buttonsContent[index] != "")
                        {
                            buttonSample.Content = buttonsContent[0];
                            wrapPanel.Children.Add(buttonSample);
                        }
                    }
                }
            }
            catch { }

            try
            {
                foreach (Button btn in wrapPanel.Children)
                {
                    if (btn != null)
                    {
                        if (btn.ActualWidth > sizeMaxButton.Width)
                        {
                            sizeMaxButton.Width = (double)btn.ActualWidth;
                        }
                        if (btn.ActualHeight > sizeMaxButton.Height)
                        {
                            sizeMaxButton.Height = (double)btn.ActualHeight;
                        }
                    }
                }
            }
            catch { }

            return sizeMaxButton;
        }
    }
}
