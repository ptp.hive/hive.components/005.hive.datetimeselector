﻿using System;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_ByteArrayToBitmapImage : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            BitmapImage returnValue = new BitmapImage();

            try
            {
                if (value != null)
                {
                    returnValue = Converter_BitmapSourceToByteArray.ConvertByteArrayToBitmapImage((byte[])value);
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from BitmapImage back to ByteArray");
        }
    }
}
