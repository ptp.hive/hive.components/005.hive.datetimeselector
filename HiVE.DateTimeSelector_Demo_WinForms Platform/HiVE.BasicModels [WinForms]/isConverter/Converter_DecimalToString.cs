﻿using System;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_DecimalToString : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            string returnValue = "0.00";

            try
            {
                if (value != null)
                {
                    if ((decimal)value < 0)
                    {
                        returnValue =
                            string.Format(
                                "- {0}",
                                Math.Abs((decimal)value));
                    }
                    else
                    {
                        returnValue = value.ToString();
                    }
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from String back to Decimal");
        }
    }
}
