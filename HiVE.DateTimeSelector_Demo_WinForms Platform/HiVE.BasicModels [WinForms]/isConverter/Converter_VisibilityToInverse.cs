﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_VisibilityToInverse : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Visible;

            try
            {
                if (value != null)
                {
                    if ((Visibility)value == Visibility.Visible)
                    {
                        returnValue = Visibility.Collapsed;
                    }
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from NotVisibility back to Visibility");
        }
    }
}
